;(function(){

function checkAndRefresh(rid, uri) {
    var bluga_id = "#bluga-waiting-" + rid;
    if ($(bluga_id).length > 0) {
        $.get(uri, null, function(data) {
            $(bluga_id).after(data)
            $(bluga_id).remove();
        });
        
        Drupal.refreshBlugaWebThumb(rid, uri);
    }
}

Drupal.refreshBlugaWebThumb = function(rid, uri) {
    setTimeout(function() { checkAndRefresh(rid, uri); }, 30000);
};

})();
