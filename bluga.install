<?php

/**
 * @file
 * Installation and schema definitions.
 */

function bluga_schema() {
  $schema['bluga_request'] = array(
    'description' => 'All the information you ever wanted to store about a Bluga API request.',
    'fields' => array(
      'rid' => array(
        'description' => 'request ID',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),

      'url' => array(
        'description' => 'The URL to create a thumbnail for.',
        'type' => 'varchar',
        'length' => 150,
        'not null' => TRUE,
      ),
      'output_type' => array(
        'description' => 'The type of file to output: jpg or png.',
        'type' => 'varchar',
        'length' => 3,
        'not null' => TRUE,
        'default' => 'jpg',
      ),
      'browser_width' => array(
        'description' => 'Width of the browser to display the URL.',
        'type' => 'int',
        'size' => 'small',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'browser_height' => array(
        'description' => 'Height of the browser to display the URL.',
        'type' => 'int',
        'size' => 'small',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default'  => 0,
      ),
      'fullthumb' => array(
        'description' => 'If true, also creates a fullsize thumb.',
        'type' => 'int',
        'size' => 'tiny',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'custom_height' => array(
        'description' => 'Custom thumbnail height.',
        'type' => 'int',
        'size' => 'small',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'custom_width' => array(
        'description' => 'Custom thumbnail width.',
        'type' => 'int',
        'size' => 'small',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'effect' => array(
        'description' => 'Apply a special effect to the thumbnail.',
        'type' => 'varchar',
        'length' => 10,
        'not null' => TRUE,
        'default' => '',
      ),
      'delay' => array(
        'description' => 'Number of seconds to wait between loading the URL and taking the thumbnail.',
        'type' => 'int',
        'size' => 'tiny',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 3,
      ),

      // These next ones only matter when thumbnail size "excerpt" is set
      'excerpt_x' => array(
        'description' => 'Only grab pixels right of this.',
        'type' => 'int',
        'size' => 'small',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'excerpt_y' => array(
        'description' => 'Only grab pixels below this.',
        'type' => 'int',
        'size' => 'small',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'excerpt_width' => array(
        'description' => 'Only grab this many pixels wide.',
        'type' => 'int',
        'size' => 'small',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 400,
      ),
      'excerpt_height' => array(
        'description' => 'Only grab this many pixels tall.',
        'type' => 'int',
        'size' => 'small',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 150,
      ),

      // Stuff Bluga tells us about this request
      'job_id' => array(
        'description' => 'Job ID assigned by the Bluga WebThumbs service.',
        'type' => 'varchar',
        'length' => 15,
        'not null' => TRUE,
      ),
      'estimate' => array(
        'description' => 'The # of seconds Bluga thinks it will take.',
        'type' => 'int',
        'size' => 'small',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'submission_time' => array(
        'description' => 'Submission time returned by Bluga.',
        'type' => 'varchar',
        'length' => 20,
        'not null' => TRUE,
        'default' => '',
      ),
      'cost' => array(
        'description' => 'The number of tokens Bluga charged for this.',
        'type' => 'int',
        'size' => 'tiny',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 1,
      ),

      // What to do when finished
      'pickup_url' => array(
        'description' => 'Where to fetch the thumbnail from.',
        'type' => 'varchar',
        'length' => 150,
        'not null' => TRUE,
        'default' => '',
      ),
      'completion_time' => array(
        'description' => 'When Bluga finished it.',
        'type' => 'varchar',
        'length' => 20,
        'not null' => TRUE,
        'default' => '',
      ),

      // Names of the locally cached files
      'filename_small' => array(
        'description' => 'The small thumbnail.',
        'type' => 'varchar',
        'length' => 100,
        'not null' => TRUE,
        'default' => '',
      ),
      'filename_medium' => array(
        'description' => 'The medium thumbnail.',
        'type' => 'varchar',
        'length' => 100,
        'not null' => TRUE,
        'default' => '',
      ),
      'filename_medium2' => array(
        'description' => 'The medium2 thumbnail.',
        'type' => 'varchar',
        'length' => 100,
        'not null' => TRUE,
        'default' => '',
      ),
      'filename_large' => array(
        'description' => 'The large thumbnail.',
        'type' => 'varchar',
        'length' => 100,
        'not null' => TRUE,
        'default' => '',
      ),
      'filename_excerpt' => array(
        'description' => 'The excerpt thumbnail.',
        'type' => 'varchar',
        'length' => 100,
        'not null' => TRUE,
        'default' => '',
      ),
      'filename_full' => array(
        'description' => 'The full "thumbnail."',
        'type' => 'varchar',
        'length' => 100,
        'not null' => TRUE,
        'default' => '',
      ),
      'filename_custom' => array(
        'description' => 'The custom thumbnail.',
        'type' => 'varchar',
        'length' => 100,
        'not null' => TRUE,
        'default' => '',
      ),
      'filename_effect' => array(
        'description' => 'The special effect thumbnail.',
        'type' => 'varchar',
        'length' => 100,
        'not null' => TRUE,
        'default' => '',
      ),
    ),

    'primary key' => array('rid'),
  );

  return $schema;
}

function bluga_install() {
  drupal_install_schema('bluga');
}

function bluga_update_6100() {
  $ret = array();

  db_drop_primary_key($ret, 'bluga_request');
  db_change_field($ret, 
    'bluga_request', 'rid', 'rid', array(
      'type' => 'serial', 
      'unsigned' => TRUE, 
      'not null' => TRUE,
    ),
    array(
      'primary key' => array('rid'),
    )
  );

  return $ret;
}
