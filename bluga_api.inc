<?php

/**
 * @file
 * This file defines the aspects of the Bluga API that aren't directly apart of
 * the Bluga module API. You may use these methods independently of the API if
 * you want to access the Bluga interface directly.
 *
 * I have implemented all of these from scratch based upon the API documentation
 * on the Bluga web site:
 *
 *   http://webthumb.bluga.net/api
 *
 * I have done this so that I could include the API for use with the module (it
 * is normally considered uncouth by the Drupal community to include 3rd Party
 * libraries in your modules) and because none of the modules came under the
 * GPL, but the LGPL.
 *
 * Besides, the API is pretty simple and it was a good way to get to know what I
 * could and could not do. :-p
 */

define('BLUGA_API_END_POINT', 'http://webthumb.bluga.net/api.php');

function _bluga_post($payload) {
  $headers = array('Content-Type' => 'text/xml');
  $message = '<webthumb><apikey>' . variable_get('bluga_api_key', '') . '</apikey>';
  $message .= $payload . "</webthumb>";
  return drupal_http_request(BLUGA_API_END_POINT, $headers, 'POST', $message);
}

function _bluga_parse_request($response) {
  $req_dom = new DOMDocument();
  $req_dom->loadXML($response->data);

  $jobs_dom = $req_dom->getElementsByTagName('job');

  $jobs = array();
  foreach ($jobs_dom as $job_dom) {
    $job_id = $job_dom->textContent;
    $jobs[$job_id] = array(
      'id' => $job_id,
      'estimate' => $job_dom->getAttribute('estimate'),
      'time' => $job_dom->getAttribute('time'),
      'url' => $job_dom->getAttribute('url'),
      'cost' => $job_dom->getAttribute('cost'),
    );
  }

  return $jobs;
}

/**
 * This performs a Bluga Request, which asks the Bluga server to consume one or
 * two tokens and create a thumbnail of the requested site. The request made
 * depends completely upon the options passed.
 *
 * @param $options This is an array of options that may contain the following
 * keys and values. You may note that this is very similar to the list of
 * options that are passed by bluga_webthumb().
 * <ul>
 * <li>"url": This is the only required parameter. This is the URL you wish to
 * take a screenshot of.</li>
 * <li>"output_type": This must be either "jpg" or "png". If you choose "png",
 * Bluga will charge you for 2 tokens on this request.</li>
 * <li>"browser": This value is passed as an array, taking two values:
 * <ul>
 * <li>"width": This is the width of the browser window to open for the
 * thumbnail. This must be within the range 15 to 1280.</li>
 * <li>"height": This is the height of the browser window to open for the
 * thumbnail. This must be within the range of 15 to 2048. If you use a browser
 * height greater than 1024, you will be charged 2 tokens for this request.</li>
 * </ul></li>
 * <li>"fullthumb": This is either "0" or "1". A value of "0" is the default. If
 * you set this to "1", you will have an additional thumbnail size "fullsize"
 * available, which will be a fullsize screenshot of the web site at the
 * dimensions set for the "browser" width and height.</li>
 * <li>"custom": This value is passed as an array, taking two values. By
 * specifying this option you will be charged 2 tokens. An extra thumbnail size
 * will become available for this request, named "custom". The dimensions of
 * this thumbnail will be as you specify. The values are:
 * <ul>
 * <li>"width": This is the pixel width you want for the custom thumbnail.</li>
 * <li>"height": This is the pixel height you want for the custom
 * thumbnail.</li>
 * </ul></li>
 * <li>"effect": By specifying this option you will be charged 2 tokens. You
 * will be provided an additional thumbnail size named "effect". The size of
 * that thumbnail depends upon the effect chosen. This thumbnail will always be
 * a PNG file. The value of this option must be one of the following:
 * <ul>
 * <li>"mirror": This will apply a mirror effect to the thumbnail so you will
 * see the reflection beneath the thumbnail as if it was a plate standing up
 * perpendicular to a plane of glass. The image result will be 300x361 in
 * size.</li>
 * <li>"dropshadow": This will add a drop shadow behind the image. The image
 * result will be 311x251 pixels.</li>
 * <li>"border": This will add a thin black border around the thumbnail. The
 * image result will be 302x242 in size.</li>
 * </ul></li>
 * <li>"delay": This is the delay in seconds between loading the web site and
 * taken the snapshot. This defaults to "3" and must be within the range of 1 to
 * 15 seconds.</li>
 * <li>"excerpt": This value is passed as an array and allows you to modify the
 * "excerpt" image size and location. It takes the following parameters:
 * <ul>
 * <li>"x": This is the horizontal position of the left edge of the
 * excerpt.</li>
 * <li>"y": This is the vertical position of the top edge of the excerpt.</li>
 * <li>"width": This is the pixel width of the image to build. If this value
 * exceeds "800" you will be charged 2 tokens.</li>
 * <li>"height": This is the pixel height of the image to build. If this value
 * exceeds "600" you will be charged 2 tokens.</li>
 * </ul></li>
 * </ul>
 * @returns A string error message or an associative array of arrays describing
 * the success of the call. The outer array will have keys that represent the
 * job ID returned by the Bluga server and values with the returned status
 * information. As of this version of the API, there will only be one key
 * present. The inner array will have the following keys:
 * <ul>
 * <li>"id": This is the job ID assigned to the request by Bluga.</li>
 * <li>"estimate": This is the number of seconds Bluga estimates until your job
 * will be ready for fetching. It is recommended in the API docs that you wait
 * at least half of this time before attempting to check the status of your
 * request.</li>
 * <li>"time": This is the time (in the US/Mountain time zone) that your request
 * was received.</li>
 * <li>"url": This is the URL that was requested.</li>
 * <li>"cost": This is the number of tokens it cost to make this request. This
 * will normally be 1, but may be 2 if you use one or more extra features that
 * cost an additional token. It should not be more than 2 unless the API has
 * changed since this writing.</li>
 * </ul>
 */
function bluga_request($options) {
  $payload .= "<request>";
  foreach ($options as $option => $value) {
    switch ($option) {
      case 'custom':
        $payload .= '<customThumbnail';
        $payload .= ' width="' . $value['width'] . '"';
        $payload .= ' height="' . $value['height'] . '"';
        $payload .= '/>';
        break;
      case 'excerpt':
        $payload .= "<$option>";
        $payload .= "<x>" . $value['x'] . "</x>";
        $payload .= "<y>" . $value['y'] . "</y>";
        $payload .= "<height>" . $value['height'] . "</height>";
        $payload .= "<width>" . $value['width'] . "</width>";
        $payload .= "</$option>";
        break;
      case 'browser':
        $payload .= '<width>' . $value['width'] . '</width>';
        $payload .= '<height>' . $value['height'] . '</height>';
        break;
      case 'output_type':
        $option = 'outputType';
      default:
        $payload .= "<$option>$value</$option>";
    }
  }
  $payload .= "</request>";
  
  $response = _bluga_post($payload);

  if ($response->error) {
    return $response->data;
  }
  else {
    return _bluga_parse_request($response);
  }
}

function _bluga_parse_status($response) {
  $stat_dom = new DOMDocument();
  $stat_dom->loadXML($response->data);

  $status_dom = $stat_dom->getElementsByTagName('status');

  $status = array();
  foreach ($status_dom as $state_dom) {
    $job_id = $state_dom->getAttribute('id');
    $status[ $job_id ] = array(
      'id' => $job_id,
      'submissionTime' => $state_dom->getAttribute('submissionTime'),
      'browserWidth' => $state_dom->getAttribute('browserWidth'),
      'browserHeight' => $state_dom->getAttribute('browserHeight'),
      'pickup' => $state_dom->getAttribute('pickup'),
      'completionTime' => $state_dom->getAttribute('completionTime'),
      'inProcess' => $state_dom->getAttribute('inProcess'),
      'message' => $state_dom->textContent,
    );
  }

  return $status;
}

/**
 * This function allows you to check the status of an existing request.
 *
 * @param $jobs_urls This is an array of URLs or Job IDs (depending on the value
 * of $type) that you wish to check the status of. This may also be a single Job
 * ID or URL rather than an array.
 * @param $type This is the type of status check to make. This must by one of
 * the two following values:
 * <ul>
 * <li>"job": All the elements being checked are listed by Job ID</li>
 * <li>"url": All the elements being checked are listed by URL</li>
 * </ul>
 * @returns An associative array of arrays. The outer array is keyed using the
 * Job ID of each status item returned. The inner values are a list of options
 * returned for each item whose status was requested. The keys will be:
 * <ul>
 * <li>"id": The Job ID of the request.</li>
 * <li>"submissionTime": The date (in the US/Mountain time zone) that the
 * request was originally received.</li>
 * <li>"browserWidth": The width of the browser that was requested (as per the
 * $options['browser']['width'] option in original request).</li>
 * <li>"browserHeight": The height of the browser that was requested (as per the
 * $options['browser']['height'] option in the original request).</li>
 * <li>"pickup": A URL where all the files built for the request can be fetched
 * in a zip file.</li>
 * <li>"completionTime": This is set only if the request has been handled and is
 * ready for pickup. This is the date (in the US/Mountain time zone) that the
 * request was completed.</li>
 * <li>"inProcess": This is set to "1" if the request is currently being
 * processed or "0" otherwise.</li>
 * <li>"message": This is the message returned for the job and will be set to
 * "completed" if the job has finished.</li>
 * </ul>
 */
function bluga_status($jobs_urls = array(), $type = 'job') {
  if (!is_array($jobs_urls)) {
    $jobs_urls = array($jobs_urls);
  }

  if (count($jobs_urls) == 0) {
    $payload = "<status/>";
  }
  else {
    $payload = '<status>';
    foreach ($jobs_urls as $job_url) {
      $payload .= "<$type>" . $job_url . "</$type>";
    }
    $payload .= '</status>';
  }

  $response = _bluga_post($payload);

  if ($response->error) {
    return $response->data;
  }
  else {
    return _bluga_parse_status($response);
  }
}

/**
 * This performs an API call which will fetch the image or images that have been
 * generated for a request from the Bluga server.
 *
 * @param $job_url This is the Job ID or URL of the request you wish to fetch.
 * If you want to pass a URL, make sure to set $type appropriately.
 * @param $size This is the kind of thumb you wish to fetch. It must be one of
 * the following values:
 * <ul>
 * <li>"small": Fetch the small 80x60 image.</li>
 * <li>"medium": Fetch the medium 160x120 image.</li>
 * <li>"medium2": Fetch the medium 320x240 image.</li>
 * <li>"large": Fetch the large 640x480 image.</li>
 * <li>"excerpt": Fetch the excerpt image (usually 400x150, but can be
 * customized during the request).</li>
 * <li>"full": Fetch the fullsize image. This is only available if the
 * "fullthumb" option is set during the original request.</li>
 * <li>"custom": Fetch a custom sized thumbnail. This is only available if the
 * "custom" option is set during the original request.</li>
 * <li>"effect": Fetch a thumbnail with a special effect applied to it. This is
 * only available if the "effect" option is set during the original
 * request.</li>
 * <li>"zip": Fetch a zip file containing all the images generated.</li>
 * </ul>
 * @param $type This is the job type. It is used to specify what kind of value
 * was given in the $job_url parameter. It must be one of the following options:
 * <ul>
 * <li>"job": The $job_url is a Job ID. This is the default.</li>
 * <li>"url": The $job_url is a URL.</li>
 * </ul>
 * @returns A $reponse object created by drupal_http_request(). The file
 * returned will be available in $response->data as long as an error did not
 * occur (i.e., $response->error is not set).
 */
function bluga_fetch($job_url, $size = 'medium', $type = 'job') {
  $payload = '<fetch>';
  $payload .= "<$type>" . $job_url . "</$type>";
  $payload .= '<size>' . $size . '</size>';
  $payload .= '</fetch>';

  $response = _bluga_post($payload);

  return $response;
}

function _bluga_parse_credit_status($response) {
  $stat_dom = new DOMDocument();
  $stat_dom->loadXML($response->data);

  return array(
    'used' => $stat_dom->getElementsByTagName('used-this-month')->item(0)->textContent,
    'cached' => $stat_dom->getElementsByTagName('easythumb-cached-this-month')->item(0)->textContent,
    'subscription' => $stat_dom->getElementsByTagName('subscription')->item(0)->textContent,
    'reserve' => $stat_dom->getElementsByTagName('reserve')->item(0)->textContent,
  );
}

/**
 * This checks the status of your Bluga account.
 *
 * @returns An array describing your current account status. The following keys
 * will be available:
 * <ul>
 * <li>"used": The number of credits you have used on your account this
 * month.</li>
 * <li>"cached": The number of easythumb thumbnails were served this month.</li>
 * <li>"subscription": The number of expiring credits you receive each month
 * (usually 100).</li>
 * <li>"reserve": The remaining number of non-expiring credits that have been
 * purchased for your account.</li>
 * </ul>
 */
function bluga_credit_status() {
  $payload = '<credits/>';

  $response = _bluga_post($payload);

  if ($response->error) {
    return $response->data;
  }
  else {
    return _bluga_parse_credit_status($response);
  }
}
